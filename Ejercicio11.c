#include <pthread.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <unistd.h>
#include <signal.h>

#define N 40


char buffer1[N], buffer2[N];                                    // buffers ping-pong (memoria compartida)
FILE *pro_FILE, *con_FILE;                                      // manejadores de archivos
pthread_t tid[3];                                               // vector para almacenar ids de los hilos

int flag_EOF = 0;                                               // flag para indicar que se leyó EOF
int flag_cons = 0, flag_prod = 0;                               // flags para indicar que se han creado hilos productores y consumidores
int flag_B2 = 0, flag_B1=0;                                     // flag para indicar que se debe leer el buffer1 o 2
int flag_cambio = 0;                                            // flag para cambiar de productor
pthread_t pid_mutex = 0 ;                                       // variable para almacenar el id del hilo productor ejecutandose

pthread_mutex_t lock_p1, lock_p2;                               // mutex de buffer 1 y 2
pthread_mutex_t lock_cambio;                                    // mutex cambio de productor

//handler para cambiar de hilo productor
void handler_cambio(int sig)
{
    flag_cambio = 1;
}


void *productor (void* Np)
{
    int j;

    char header[3]="P_:";

    if ((*(int*)Np) == 1)                                       // header del hilo 1
        header[1] = '1';
    else                                                        // header del hilo 2 
        header[1] = '2';
    

    while ( flag_EOF == 0 )                                     // mientras no se haya llegado al final del archivo
    {
        // Control de Productores
        if(pid_mutex != pthread_self())
        {
            pthread_mutex_lock(&lock_cambio);

            pid_mutex = pthread_self();
        }

        if (flag_EOF == 1)
        {
            pthread_mutex_unlock(&lock_cambio);
            return NULL;                                        // Salida del hilo inactivo
        }
        

        /*----BUFFER 1----*/

        pthread_mutex_lock(&lock_p1);                           // activo mutex de buffer1
        flag_B1 = 1;                                            // indico al consumidor que no omita el buffer1
        memset(buffer1,'\0',sizeof(buffer1));                   // limpio buffer1

        sprintf(buffer1,"%s",header);                           // agrego header al principio

        // leo una linea de pg2000.txt  y la escribo en buffer1
        for (j = 3;j<N;j++)
        {
            if ((buffer1[j] = fgetc(pro_FILE)) == EOF)
            {
                buffer1[j]='\0';                                // El caracter leido en forma incorrecta se remplaza por caracter nulo
                flag_EOF = 1;
                pthread_mutex_unlock(&lock_cambio);
                j = N + 1;                                      // Para salir del ciclo for 
            }

            else if (buffer1[j] == '\n')
            {
                j = N + 1;
            }                
        }

        printf ("+[B1]: %s\n", buffer1);
        pthread_mutex_unlock(&lock_p1);                         // libero mutex de buffer1 para que el consumidor pueda leerlo

        flag_prod = 1;                                          // Flag para notificar a consumidor de que ya se escribió por primera vez el buffer1 
        sleep(1);

        /*----BUFFER 2----*/

        if (j == N)                                             // si se escribió el buffer1 completo
        {
            pthread_mutex_lock(&lock_p2);                       // activo mutex de buffer2
            flag_B2 = 1;                                        // indico al consumidor que no omita el buffer2
            memset(buffer2,'\0',sizeof(buffer2));               // limpio buffer

            // termino de leer la linea de pg2000.txt  y la escribo en buffer2
            for (j = 0;j<N;j++) 
            {
                if ((buffer2[j] = fgetc(pro_FILE)) == EOF)
                {
                    buffer2[j]='\0';
                    flag_EOF = 1;
                    pthread_mutex_unlock(&lock_cambio);
                    j = N + 1;
                }

                else if (buffer2[j] == '\n')
                {
                    j = N + 1;
                }                
            }

            printf ("+[B2]: %s\n", buffer2);
            pthread_mutex_unlock(&lock_p2);                     // libero mutex de buffer2 
        }

        while (flag_cons == 0) {}                               // espero a que se cree un consumidor para no sobreescribir información que no fue consumida aún

        if (flag_cambio == 1)                                   // si se activa el flag para cambiar de productor
        {
            pthread_mutex_unlock(&lock_cambio);
            flag_cambio = 0;
        }

        sleep (1);                                              // espera para que no se pisen los hilos prod y cons
    } 
    return NULL;
}


void *consumidor (void* arg)
{
    int flag_ul = 0;                                            // flag para limitar la lectura a una última iteración
    while (flag_prod == 0){}                                    // espero a que se cree un productor para no leer el buffer sin que se modifique su contenido
    flag_cons = 1;                                              // flag para notificar al productor que ya existe un proceso consumidor que lea el buffer
    
    while (flag_ul == 0)
    {
        /*----BUFFER 1----*/
        if ((flag_B1 == 1) && (flag_B2 == 0))
        {
            flag_B1 = 0;                                        // reseteo flag
            pthread_mutex_lock(&lock_p1);                       // activo mutex de buffer1

            printf("\n-[B1]: %s\n",buffer1);

            if(EOF==fputs(buffer1,con_FILE))                    // guardo contenido de buffer1 en el archivo
            {
                printf("\n[ERROR B1]\n");
            }

            pthread_mutex_unlock(&lock_p1);                     // desbloqueo mutex de buffer1
        }
        
        /*----BUFFER 2----*/
        if (flag_B2 == 1)                                       // si se escribió en el buffer2
        {
            flag_B2 = 0;                                        // reseteo flag
            pthread_mutex_lock(&lock_p2);                       // activo mutex de buffer2
            
            printf("\n-[B2]: %s\n",buffer2);

            if(EOF==fputs(buffer2,con_FILE))                    // guardo contenido de buffer2 en el archivo
            {
                printf("\n[ERROR B2]\n");
            }

            pthread_mutex_unlock(&lock_p2);                     // desbloqueo mutex de buffer2
        }

        if (flag_EOF == 1)                                      // si se detecta EOF se modifica la variable de control del bucle
        {
            flag_ul = 1;
        }
        
        usleep(1);                                              // demora para que no se escriba varias veces la misma linea
    }

    return NULL;
}


int main (void)
{
     printf("===== Programa inciado PID:%d =====\n\n",getpid());
    int Np[] = {1,2}; 

    // Apertura de archivos
    if ((pro_FILE=fopen("pg2000.txt","r")) == NULL)           // Archivo de entrada
    {
        perror("Error al Abrir el archivo\n");
        exit(1);
    }

    fseek(pro_FILE,0,SEEK_SET);

    if ((con_FILE=fopen("salida.txt","w")) == NULL)           // Archivo de salida
    {
        perror("Error al Abrir el archivo\n");
        exit(1);
    }

    // Inicio de manejador de señales
    signal(SIGUSR1,handler_cambio);

     // Inicio el mutex
    pthread_mutex_init(&lock_p1,NULL);                          // Lock buffer1
    pthread_mutex_init(&lock_p2,NULL);                          // Lock buffer2
    pthread_mutex_init(&lock_cambio, NULL);                     // Lock para productor en espera

    // inicio hilos
    if(pthread_create(&(tid[0]), NULL, &productor, Np)!=0)      // hilo productor 1
    {
        perror("No se pudo crear el hilo\n");
        exit(1);
    }

    if(pthread_create(&(tid[2]), NULL, &consumidor, NULL)!=0)   // hilo consumidor
    {
        perror("No se pudo crear el hilo\n");
        exit(1);
    }

    if(pthread_create(&(tid[1]), NULL, &productor, Np+1)!=0)    // hilo productor 2
    {
        perror("No se pudo crear el hilo\n");
        exit(1);
    }

    // finalización de programa
    pthread_join(tid[0],NULL);                                  // espero a que finalice productor 1
    pthread_join(tid[1],NULL);                                  // espero a que finalice productor 2
    pthread_join(tid[2],NULL);                                  // espero a que finalice consumidor

    pthread_mutex_destroy(&lock_p1);                            // destrucción de mutex para buffer1
    pthread_mutex_destroy(&lock_p2);                            // destrucción de mutex para buffer2
    pthread_mutex_destroy(&lock_cambio);                        // destrucción de mutex para cambio de productor

    // Cerrado de archivos
    if(fclose(pro_FILE)!=0)
    {
        perror("Error al cerrar el archivo\n");
        exit(1);
    }

    if(fclose(con_FILE)!=0)
    {
        perror("Error al cerrar el archivo\n");
        exit(1);
    }

    return 0;
}